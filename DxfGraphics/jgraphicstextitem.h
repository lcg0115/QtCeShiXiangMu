﻿#ifndef JGRAPHICSTEXTITEM_H
#define JGRAPHICSTEXTITEM_H

#include <QColor>
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneWheelEvent>
#include <QStyleOptionGraphicsItem>

class JGraphicsTextItem : public QGraphicsItem
{
public:
    JGraphicsTextItem(const QString &text, int x, int y);

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) override;

    QString text() const { return m_text; }
    void setText(const QString &text);

    QColor color() const { return m_color; }
    void setColor(const QColor &color);

    void setX(int x);
    void setY(int y);

private:
    QString m_text;
    int x;
    int y;
    QColor m_color;
};

#endif // JGRAPHICSTEXTITEM_H
