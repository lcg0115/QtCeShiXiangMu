﻿#ifndef SAMPDATAFORMAT_H
#define SAMPDATAFORMAT_H

#include <QtCore>
#include <QFile>

class DataHeader
{
public:
    char		bfType[4];              //采样数据标识号"VIPC"
    long		bfSize;					//文件大小
    short		diVer; 					//版本号
    short		diADBit;				// AD精度
    long		diSize;					//采样数据大小
    double		diSampleFreq;			//采样频率
    short		diChannelNum; 			//总通道数
    short		diCh; 					//采样通道
    float		diSensitivity;			//传感器灵敏度
    char		diTestPointNum[10];     //测点号
    int			diMultiple; 			//放大倍数
    char		diUnit[20];             //工程单位
    char		diRemark[256];          //备注
    char		diDataFlag[4];          //数据开始位置标识"data"(0x61746164)
    DataHeader();
    DataHeader(const char* type,
               long fsize,
               short ver,
               short ADBit,
               long dSzie,
               double freq,
               short chNum,
               short ch,
               float sen,
               const char* pointNum,
               int multiple,
               const char* unit = "mV",
               const char* remark = "reamrk",
               const char*  flag = "data");
    bool operator==(const DataHeader &other) const;
};

class SampDataFormat
{
public:
    SampDataFormat();
    ~SampDataFormat();
    bool ReadDataFile(const QString &FilePath);
public:
    int HeaderSize;
    DataHeader Header;
    float *Data;
};

#endif // SAMPDATAFORMAT_H
