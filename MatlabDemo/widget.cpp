﻿#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QLibrary>
#include <mclmcrrt.h>
#include <mclcppclass.h>
#include "derivative.h"
#include "add.h"


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    addInitialize();
    mxArray* a = mxCreateDoubleMatrix(1, 1, mxREAL);
    mxArray* b = mxCreateDoubleMatrix(1, 1, mxREAL);
    mxDestroyArray(a);
    mxDestroyArray(b);
//    add(1, c, a, b);
    addTerminate();
}
