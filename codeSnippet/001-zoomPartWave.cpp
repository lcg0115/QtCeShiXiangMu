1. 首先需要实现鼠标的点击、移动、释放事件
2. 再到事件里面实现，实现时使用的QRubberBand类

#include <QRubberBand>
xxx.h中
private:
    QRubberBand *RubBand;

xxx.cpp中
SignalWave::SignalWave(const int &Ch, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SignalWave),
    RubBand(new QRubberBand(QRubberBand::Rectangle, this)), // 使用初始化列表初始化RubBand变量
    ChNumber(Ch)
{
    ui->setupUi(this);
}


void SignalWave::mousePressEvent(QMouseEvent *e)
{
    if(!ui->SglWave->viewport().contains(e->pos())) return;
    if(e->button() == Qt::LeftButton) {
        if(!RubBand->isVisible()) {
            StartPoint = e->pos();
            StartPoint.setY(0);
            RubBand->setGeometry(QRect(StartPoint, QSize()));
            RubBand->show();
        }
    }
    else if(e->button() == Qt::MidButton) {
        ui->SglWave->rescaleAxes();
        ui->SglWave->replot();
    }
}

void SignalWave::mouseMoveEvent(QMouseEvent *e)
{
    if(!ui->SglWave->viewport().contains(e->pos())) return;
    if(RubBand->isVisible()) {
        QPoint EndPoint = e->pos();
        EndPoint.setY(ui->SglWave->height());
        RubBand->setGeometry(QRect(StartPoint, EndPoint).normalized());
    }
}

void SignalWave::mouseReleaseEvent(QMouseEvent *e)
{
    if(!ui->SglWave->viewport().contains(e->pos())) return;
    if(e->button() == Qt::LeftButton) {
        const QRect zoomRect = RubBand->geometry();
        int xp1, yp1, xp2, yp2;
        zoomRect.getCoords(&xp1, &yp1, &xp2, &yp2);
        double x1 = ui->SglWave->xAxis->pixelToCoord(xp1);
        double x2 = ui->SglWave->xAxis->pixelToCoord(xp2);
        ui->SglWave->xAxis->setRange(x1, x2);
        RubBand->hide();
        ui->SglWave->replot();
    }
}